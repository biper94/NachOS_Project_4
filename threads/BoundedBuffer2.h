#ifndef BOUNDEDBUFFER2_H
#define BOUNDEDBUFFER2_H
#include "synch.h"
#include "synchlist.h"
using namespace std;

class BoundedBuffer2 {
public:
// create a bounded buffer with a limit of 'maxsize' bytes
    BoundedBuffer2(int maxsize);
    //~BoundedBuffer2();
    // read 'size' bytes from the bounded buffer, storing into 'data'.
    // ('size' may be greater than 'maxsize')
    int Read(char *data, int size, int threadNum);
    // write 'size' bytes from 'data' into the bounded buffer.
    // ('size' may be greater than 'maxsize')
    int Write(char *data, int size, int threadNum);
    static void SelfTestHelper_R(int threadNum);
    static void SelfTestHelper_W(int threadNum);
    void SelfTest(int argc, char** argv);
private:
    //void *m_buffer;
    SynchList<char>* m_buffer;
    Lock *m_lock;

    int m_bufferSize;

    int m_count; // current buffer size
    int m_nextin;
    int m_nextout;

    Condition *m_bufferEmpty;
    Condition *m_bufferFull;

};
#endif
