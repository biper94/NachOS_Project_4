#ifndef SLEEPINGTHREAD_H
#define SLEEPINGTHREAD_H

#include "thread.h"
#include <list>

class SleepingThread {
    Thread* t;
    int when;
public:
    SleepingThread(Thread* _t, int _when)
        : t(_t), when(_when) {};
};

#endif
