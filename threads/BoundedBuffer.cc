#include "BoundedBuffer.h"
#include "thread.h"
#include <cstdlib>
#include <ctime>
#include <string>
#include <sstream>
#include <string.h>

BoundedBuffer* bBuffer;

#define BOUNDBUFFERDEBUG 0

BoundedBuffer::BoundedBuffer(int _maxSize)
    : maxSize(_maxSize)
    , currentSize(0)
    , isEnabled(true){
    buffer = new SynchList<char>;
    mutex = new Semaphore("mutex", 1);
    canReadSem = new Semaphore("canReadSem", 0);
    canWriteSem = new Semaphore("canWriteSem", _maxSize);
}

int BoundedBuffer::Read(char* data, int size, int threadNum) {
    //Read size bytes from the buffer, blocking as necessary
    //until enough bytes are available to completely satisfy the request.
    //Copy the bytes into memory starting at address data.
    //Return the number of bytes successfully read.
    int i = 0;
    if (currentSize == 0)
        cout << "***Thread No." << threadNum << " Buffer is Empty!!" << endl;

    canReadSem->P();
    mutex->P();
    while (i < size) {

        if (isEnabled) {
            data[i] = buffer->RemoveFront();

            i++;
            currentSize--;
        }
        else {
            i = 0;
            break;
        }
    }

    mutex->V();
    canWriteSem->V();

    return i;
}

int BoundedBuffer::Write(char* data, int size, int threadNum) {
    //Write size bytes into the buffer, blocking as necessary
    //until enough space is available to completely satisfy the request.
    //Copy the bytes from memory starting at address data .
    //Return the number of bytes successfully written.

    int i = 0;
    if (currentSize == maxSize)
        cout << "***Thread No." << threadNum << " Buffer is Full!!" << endl;
    canWriteSem->P();
    mutex->P();
    while (i < size) {

        if (isEnabled) {
            //buffer[currenBuffertSize++] = data[i];
            buffer->Append(data[i]);
            currentSize++;
            i++;
        }
        else {
            i = 0;
            break;
        }
    }
    mutex->V();
    canReadSem->V();
    return i;
}

void BoundedBuffer::Close() {
    //Permanently close the BoundedBuffer object.
    //This releases any blocked readers or writers;
    //any subsequent attempts to Read or Write the buffer fail and return 0.
    mutex->P();
    isEnabled = false;
    mutex->V();
}


void BoundedBuffer::SelfTestHelper_R(int threadNum) {
    //srand((unsigned int)time(NULL));
    int size = (int)rand() % 10;
    //cout << "\t\t\t\t\t\tThread No." << threadNum << "\'s reading randome size: "
    //    << size << endl;
    char* buff = new char[20];
    int returnSize = bBuffer->Read(buff, size, threadNum);
    cout << "\t\t\t\t\t\t***Thread NO." << threadNum << " Read "
        << returnSize << endl;
}


void BoundedBuffer::SelfTestHelper_W(int threadNum) {
    //srand((unsigned int)time(NULL));
    int size = (int)rand() % 19;
    char* buf = new char[30];
    for (int i = 0; i < size; i++) {
        buf[i] = (char)((rand() % 25) + 65);
    }
    buf[size] = '\0';
    //cout << "***Thread No." << threadNum << " Writing BUF: " << buf << endl;
    int returnSize = bBuffer->Write(buf, size, threadNum);


    cout << "***Thread NO." << threadNum << " Write "
        << returnSize << endl;
}

void BoundedBuffer::SelfTestHelper_C(int threadNum) {

    bBuffer->Close();

    cout << "**********Thread No." << threadNum << " Buffer closed ***********************" << endl;
}
void BoundedBuffer::SelfTest(int argc, char** argv) {
    
    bBuffer = this;
    bool Close = false;
    int numOfTh = 0;
    for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i], "-b") == 0) {
            numOfTh = atoi(argv[i + 1]);
            cout << "number of thread: " << numOfTh << endl;
            break;

        }
        if (strcmp(argv[i], "-db") == 0) {
            Close = true;
            numOfTh = atoi(argv[i + 1]);
            cout << "number of thread: " << numOfTh << endl;
            break;
        }
    }
    for (int i = 0; i < numOfTh; i++) {

        Thread* t = new Thread("");

        if (i % 2)
            t->Fork((VoidFunctionPtr)SelfTestHelper_R, (void*) i);
        else
            t->Fork((VoidFunctionPtr)SelfTestHelper_W, (void*) i);

    }
    if (Close) {
        Thread* t = new Thread("");
        t->Fork((VoidFunctionPtr)SelfTestHelper_C, (void*) (numOfTh + 1));
    }

}
