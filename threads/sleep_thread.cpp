#include "sleep_thread.h"
#include "utility.h"
#include "main.h"

void Sleep_thread::PutToSleep(Thread* t, int x) {

    ASSERT(kernel->interrupt->getLevel() == IntOff);

    //cout << "**Thread " << t << " will wake up at interrupt " << _current_interrupt + x << "." << endl;
    sleep_thread_list.push_back( Sleeping(t, _current_interrupt + x));

    t->Sleep(false);
}

bool Sleep_thread::WakeUpThreads() {
    bool waken = false;

    ++_current_interrupt;

    if (_current_interrupt % 10 == 0) {
        //cout << "-- current interrupt is " << _current_interrupt << endl;
    }
    std::list<Sleeping>:: iterator it;
    for (it = sleep_thread_list.begin(); it != sleep_thread_list.end();) {
        if (_current_interrupt >= it->whenToWakeUp) {
            //cout << "** Thread "<< it->SThread << " is waking up.." << endl;
            waken = true;
            kernel->scheduler->ReadyToRun(it->SThread);

            it = sleep_thread_list.erase(it);
        }
        else
            ++it;
    }

    return waken;
}
