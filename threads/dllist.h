#ifndef DLLIST_H_
#define DLLIST_H_

class DLLElement {

public:
	DLLElement(void *itemPtr, int sortKey);

	
	DLLElement *next;
	DLLElement *prev;

	int key;
	void *item;
};

class DLList {

public:
	DLList();
	~DLList();
	void RecursiveDestuctor(DLLElement*);

	void Prepend(void *item); 	// add to head of list (set key = min_key + 1)
	void Append(void *item);	// add to tail of list (set key = max_key + 1)
	void *remove(int *keyPtr);	// rmove from head of list
								// set *keyPtr to key of the removed item
								// return item (or NULL if list is empty)

	bool IsEmpty();				// return true if list has elements

	// routines to put/get items on/off list n order (sorted by key)

	void SortedInsert(void *item, int sortKey);
	void *SortedRemove(int sortkey); // remove sorted item with (sortkey == key)
									// return null if no such item exists
	void Print_char_item();

private:
	DLLElement *first;
	DLLElement *last;
};
#endif
