#ifndef BOUNDEDBUFFER_H
#define BOUNDEDBUFFER_H

#include "synch.h"
#include "synchlist.h"
class BoundedBuffer {
private:
    int maxSize;
    int currentSize;
    bool isEnabled;
    Semaphore* mutex;
    Semaphore* canWriteSem;
    Semaphore* canReadSem;
    SynchList<char> *buffer;


public:

    BoundedBuffer(int maxSize);
    int Read(char* data, int size, int threadNum);
    int Write(char* data, int size, int threadNum);
    void Close();
    static void SelfTestHelper_R(int threadNum);
    static void SelfTestHelper_W(int threadNum);
    static void SelfTestHelper_C(int threadNum);
    void SelfTest(int argc, char** argv);

};

#endif
