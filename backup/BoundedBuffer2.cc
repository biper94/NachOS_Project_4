#include "BoundedBuffer2.h"

BoundedBuffer2* bBuffer;

BoundedBuffer2::BoundedBuffer2(int maxsize){
    m_lock = new Lock("buffer lock");
    m_bufferEmpty = new Condition("bufferEmpty");
    m_bufferFull = new Condition("bufferFull");

    m_buffer = new SynchList<char>();
    //m_buffer = (void *)AllocBoundedArray(sizeof(void *) * maxsize);

    m_bufferSize = maxsize;
    m_count = 0;

}

int BoundedBuffer2::Read(char* data, int size, int threadNum){
    int i;
    m_lock->Acquire();
    while(m_count == 0)
        m_bufferFull->Wait(m_lock);
    for(i = 0; i < size; i++){
        while(m_count == 0)
        m_bufferFull->Wait(m_lock);
        //*((char *)data + i) = *((char *)m_buffer + m_nextout);
        *(data + i) = m_buffer->RemoveFront();
        if(i == size - 1)
            //printf("%d\n", size);
        m_nextout = (m_nextout + 1) % m_bufferSize;
        m_count--;
        if (m_count == 0)
            cout << "\t\t\t\t\t\tThread No." << threadNum << " Buffer is Empty!!" << endl;
        cout << "\t\t\t\t\t\tThread No." << threadNum << " current buffer size: " << m_count << endl;
        m_bufferEmpty->Signal(m_lock);
    }

    m_lock->Release();
    return size;
}
int BoundedBuffer2::Write(char *data, int size, int threadNum){
    int j;
    m_lock->Acquire();
    while(m_count == m_bufferSize)
        m_bufferEmpty->Wait(m_lock);
    for(j = 0; j < size; j++){
        while(m_count == m_bufferSize)
            m_bufferEmpty->Wait(m_lock);
        //*((char *)m_buffer + m_nextin) = *((char *)data + j);
        m_buffer->Append(*(data + j));
        //printf("%c\n",*((char *)data +j));
        if(j == size-1)
            //printf("%d\n", size);
        m_nextin = (m_nextin+1) % m_bufferSize;
        m_count++;
        if (m_count == m_bufferSize)
            cout << "Buffer is Full!" << endl;
        cout << "Thread No." << threadNum << " current buffer size: " << m_count << endl;
        m_bufferFull->Signal(m_lock);
    }
    m_lock->Release();
    return size;
}
void BoundedBuffer2::SelfTestHelper_R(int threadNum) {
    //srand((unsigned int)time(NULL));
    int size = (int)rand() % 20;
    //cout << "\t\t\t\t\t\tThread No." << threadNum << "\'s reading randome size: "
    //    << size << endl;
    char* buff = new char[20];
    int returnSize = bBuffer->Read(buff, size, threadNum);
    cout << "\t\t\t\t\t\t***Thread NO." << threadNum << " Read "
        << returnSize << endl;
}


void BoundedBuffer2::SelfTestHelper_W(int threadNum) {
    //ssrand((unsigned int)time(NULL));
    int size = (int)rand() % 19;
    char* buf = new char[30];
    for (int i = 0; i < size; i++) {
        buf[i] = (char)((rand() % 25) + 65);
    }
    buf[size] = '\0';
    //cout << "***Thread No." << threadNum << " Writing BUF: " << buf << endl;
    int returnSize = bBuffer->Write(buf, size, threadNum);


    cout << "***Thread NO." << threadNum << " Write "
        << returnSize << endl;
}

void BoundedBuffer2::SelfTest(int argc, char** argv) {
    bBuffer = this;
    for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i], "-b") == 0) {
            int numOfTh = atoi(argv[i + 1]);
            cout << "number of thread: " << numOfTh << endl;

            for (int i = 0; i < numOfTh; i++) {

                Thread* t = new Thread("");
                if (i % 2)
                    t->Fork((VoidFunctionPtr)SelfTestHelper_R, (void*) i);
                else
                    t->Fork((VoidFunctionPtr)SelfTestHelper_W, (void*) i);
            }

            break;
        }
    }
}
